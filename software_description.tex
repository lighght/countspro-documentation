Counts.pro transmits radiation counts data to a mobile device. 
The data is retained, including information about the measurement (\autoref{tab:dataset_model}), until the user chooses to store data to a remote server. 
When the data is stored remotely, it may be accessed by a web-dashboard at the counts.pro website. 

\begin{table}
  \centering
  \begin{tabular}{l l c}
    \toprule
    Metadata entry & Example & \multicolumn{1}{l}{User input} \\
    \midrule
    Dataset name & Survey 123456 & \lib{✔} \\ %  ☑ ✅ ✓
    Location GPS & 35.123456, -84.123456 \\
    Location description & Building 123 & \lib{✔} \\
    Meter model & Model 123 & \lib{✔} \\
    Meter serial & 123456789 & \lib{✔} \\
    Probe model & $2 \times 2$ NaI & \lib{✔} \\
    Probe serial & 987654321 & \lib{✔} \\
    Scaler duration & \SI{240}{\second} \\
    User ID & User123 \\
    \bottomrule
    \end{tabular}
    \vspace{1.5\onelineskip}
    
    \begin{tabular}{l l}
    \toprule
    Data entry & Example \\
    \midrule
    Timestamp & 2017-06-29 02:34:56.789012 \\
    Raw count & \num{9} \\
    EMA (count) & \num{9} \\
    Elapsed time & \SI{250}{\milli\second} \\
    GPS location & 35.123123, -84.123123 \\
    \bottomrule
  \end{tabular}
  \caption[
  Each counts.pro measurement includes many data entries, and metadata for the associated dataset. ]{
  Each counts.pro measurement includes many data entries, and metadata for the associated dataset. 
  Each data entry represents a single \SI{250}{\milli\second} count; the number of entries depends on the length of a scaler measurement. 
  The data entry EMA refers to an exponential moving average; the instantaneous count-rate is smoothed algorithmically by \autoref{eq:ema}. 
  \label{tab:dataset_model}}
\end{table}

The counts.pro hardware interface transmits using bluetooth, requiring a mobile device to receive and record the data. 
A mobile app can be obtained for IOS through the app store, which connects to the counts.pro device. 

The data that are collected by the mobile app are simply: the number of counts in the data collection cycle , and the precise value of time that elapsed in case of deviation from the expected value of of \SI{250}{\milli\second}. 
The mobile app maintains a moving-window average \nomenclature{EMA}{Exponential moving average} (exponential moving average, or EMA) of the counts data, which is displayed in $[\si{counts\per\second}]$. 
An algorithm to generate the EMA datum $\mean{c}_n$ at each data point $c_n$ is as follows:
%
\begin{equation}
  \mean{c}_n = \dfrac{(N - 1)\mean{c}_{n-1} + c_n}{N}
  \label{eq:ema}
\end{equation}
%
where $N$ determines the decay-rate of previous measurements; where a shorter $N$ gives more weight to recent measurements and longer $N$ retains the effect of measurements longer. 


\section{Mobile App
\label{sec:mobile_app}}

On first use of the mobile app, user information will be requested. 
A login is required for transferring data to centralized storage. 
Available counts.pro hardware will be identified by the app, which will ask the user to associate a model name and serial number, and a detector and serial number. 
These values can be modified at any time, but for simplicity they will become default values for future use. 

The mobile app can be used as a ratemeter and scaler display with or without data logging. 
Ratemeter data updates every \SI{250}{\milli\second}. 
The scaler function allows the user to choose any amount of time, or continuous counting. 
The option to store a scaler count as a data-file is given, as is the option to data-log a continuous measurement until terminated. 

Measurements are stored on the mobile device as named datasets. 
When a user has collected data, the option to store the datasets to a central server is given. 
The user can modify or edit any of the ``User input'' fields in \autoref{tab:dataset_model} before uploading. 
Once a dataset is uploaded, all of the stored data is made to be read-only and no further modification to metadata can be made. 
Access to the data is provided by a web dashboard. 


\section{Database
\label{sec:database}}

The data is stored according to the model provided in \autoref{tab:dataset_model}. 
For every scaler count or user-initiated data-log, a metadata entry for the entire set is recorded. 
Each \SI{250}{\milli\second} data point includes its own metatdata, as shown. 
The space on-disk for a \SI{60}{\second} count is \SI{20}{\kibi\byte}


\section{Dashboard
\label{sec:dashboard}}

The web frontend of the data storage system is a web dashboard, available at \url{https://app.counts.pro}. 
Users will sign in using the same identity as the mobile app for access to stored data. 
Data can be downloaded as a comma-delimited file for analysis and reporting. 

To use the web dashboard, you must visit \url{https://app.counts.pro} and log in with the same username and password as you use for the mobile app. 
If you do not yet have a username and password, you can sign up for an account using the mobile app (\autoref{sec:mobile_app}), or on the \url{https://app.counts.pro} website. 

Once signed in, the dashboard \emph{Home} screen (\autoref{fig:app_data}) displays a navigation panel on the left, and a brief list of the most recent datasets that have been synced to the cloud database (see \autoref{sec:mobile_app} for syncing). 
Navigate to the \emph{My Datasets} screen to access all of the datasets in your database. 
Sort data by Date, Name, and several other categories using the arrows next to each. 
Filter data using custom search terms, such as ``Daily Source Check''. 
%
\begin{figure}
  \centering
  \includegraphics[width=0.8\textwidth]{app_data}
  \caption[%
  Main page of the \url{https://app.counts.pro} dashboard. ]{
  Main page of the \url{https://app.counts.pro} dashboard. 
  \label{fig:app_data}}
\end{figure}

Select datasets that you want to download using the selection boxes next to each. 
The selected sets will be downloaded as a \texttt{.zip} file containing each dataset as a \texttt{.csv}, titled with the \emph{Name} and \emph{Date} fields. 
Give the \texttt{.zip} file a name based on its contents for easier access (\autoref{fig:app_zip}).
%
\begin{figure}
  \centering
  \includegraphics[width=0.8\textwidth]{app_zip}
  \caption[%
  Select a custom filename for the selected datasets to be downloaded as a \texttt{.zip} file. ]{
  Select a custom filename for the selected datasets to be downloaded as a \texttt{.zip} file. 
  \label{fig:app_zip}}
\end{figure}


\section{User Forum
\label{sec:user_forum}}

Users are encouraged to contribute to the user forum (\url{https://forum.counts.pro/}), where tips, troubleshooting, and feature requests can be made. 
The counts.pro team is working with the goals of the counts.pro community to bring new features and capabilities to the world of radiation measurement and monitoring. 
